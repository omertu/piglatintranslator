package plt;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "Hello World";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("Hello World",t.getPhrase());
	}

	@Test
	public void testEmptyPhrase() {
		String inputPhrase = "";
		Translator t = new Translator(inputPhrase);
		
		assertEquals(Translator.NIL,t.translate());
	}
	
	@Test
	public void testWordStartsWithVowelEndsWithY() {
		String inputPhrase = "any";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("anynay",t.translate());
	}
	
	@Test
	public void testWordStartsWithVowelEndsWithVowel() {
		String inputPhrase = "apple";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("appleyay", t.translate());
	}
	
	@Test
	public void testWordStartsWithVowelEndsWithConsonant() {
		String inputPhrase = "ask";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("askay",t.translate());
	}
	
	@Test
	public void testStartsWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay",t.translate());
	}
	
	@Test
	public void testStartsWithMoreConsonants() {
		String inputPhrase = "known";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ownknay",t.translate());
	}
	
	@Test
	public void testPhraseHaveMoreWords() {
		String inputPhrase = "hello world";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway", t.translate());
	}
	
	@Test
	public void testWordIsComposite() {
		String inputPhrase = "well-being";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay",t.translate());
	}
	
	@Test 
	public void testPhraseWithPunctuations() {
		String inputPhrase = "hello world!";
		Translator t = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!",t.translate());
	}
}
