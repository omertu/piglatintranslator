package plt;

public class Translator {

	private String phrase;
	public static final String NIL = "nil";

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	private class Word {
		private String actualWord;

		private Word(String inputWord) {
			actualWord = inputWord;
		}

		private boolean charAtPosIsConsonant(int pos) {
			final String consonants = "bcdfghijklmnpqrstvxywz";
			for (int i = 0; i < consonants.length(); i++) {
				if (actualWord.charAt(pos) == consonants.charAt(i)) {
					return true;
				}
			}
			return false;

		}

		private boolean charAtPosIsVowel(int pos) {
			String vowels = "aeiou";
			for (int i = 0; i < vowels.length(); i++) {
				if (actualWord.charAt(pos) == vowels.charAt(i)) {
					return true;
				}
			}
			return false;

		}

		private int withHowManyConsonantsStarts() {
			int consonantsNum = 0;
			for (int i = 0; i < actualWord.length(); i++) {
				if (charAtPosIsConsonant(i))
					consonantsNum++;
				if (charAtPosIsVowel(i))
					break;
			}
			return consonantsNum;
		}

		private String translateWord() {
			if (charAtPosIsVowel(0)) {
				int lastPos = actualWord.length() - 1;
				if (actualWord.endsWith("y")) {
					return actualWord + "nay";
				} else if (charAtPosIsVowel(lastPos)) {
					return actualWord + "yay";
				} else if (charAtPosIsConsonant(lastPos)) {
					return actualWord + "ay";
				}
			} else if (this.charAtPosIsConsonant(0)) {
				String firstHalf = actualWord.substring(0, withHowManyConsonantsStarts());
				String secondHalf = actualWord.substring(withHowManyConsonantsStarts(), actualWord.length());
				return secondHalf + firstHalf + "ay";
			}
			return "";
		}
	}

	private boolean isPunctuation(char c) {
		return ((c == '.') || (c == ',') || (c == ';') || (c == ':') || (c == '?') || (c == '!') || (c == '\'')
				|| (c == '(') || (c == ')') || (c == ' ') || (c == '-'));
	}

	public String translate() {

		if (phrase.length() != 0) {
			StringBuilder translatedPhrase = new StringBuilder();
			StringBuilder wordBuilder = new StringBuilder();
			for (int i = 0; i < phrase.length(); i++) {
				if (!isPunctuation(phrase.charAt(i))) {
					wordBuilder.append(phrase.charAt(i));
					if ((i == phrase.length() - 1) || isPunctuation(phrase.charAt(i + 1))) {
						Word word = new Word(wordBuilder.toString());
						translatedPhrase.append(word.translateWord());
						wordBuilder.delete(0, wordBuilder.length());
					}
				} else {
					translatedPhrase.append(phrase.charAt(i));
				}
			}
			return translatedPhrase.toString();
		} else
			return NIL;

	}
}